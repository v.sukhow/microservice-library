package com.spring;

import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
public class Controller {

    @Autowired
    private EurekaClient eurekaClient;

    @RequestMapping("/user/{userId}/take-book/{bookId}")
    public ResponseEntity<String> takeBook(@PathVariable int userId, @PathVariable int bookId) {
        int accessPort = getPort("gateway-app");

        String checkUrl = generateStartUrl(accessPort, "check-app") + "check";
        ResponseEntity<String> checkResponse = new RestTemplate().getForEntity(checkUrl, String.class);

        if (checkResponse.getBody().equals("YES")) {
            String decreaseNumberBooksInLibraryUrl = generateStartUrl(accessPort, "library-app") + "books/" + bookId + "/decrease-number";
            String increaseNumberUserBooksUrl = generateStartUrl(accessPort, "users-app") + "users/" + userId + "/increase-number-books";
            new RestTemplate().getForEntity(decreaseNumberBooksInLibraryUrl, Object.class);
            new RestTemplate().getForEntity(increaseNumberUserBooksUrl, Object.class);
            return new ResponseEntity<String>("Done", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("Failed, try again", HttpStatus.PRECONDITION_FAILED);
        }
    }

    private String generateStartUrl(int port, String nameApplication) {
        return "http://localhost:" + port + "/" + nameApplication + "/";
    }

    private int getPort(String applicationName) {
        return eurekaClient.getApplication(applicationName).getInstances().get(0).getPort();
    }

}
