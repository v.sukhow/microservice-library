package com.spring;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CheckController {
    @RequestMapping("/check")
    public String getBookById() {
        String[] options = {"YES", "NO"};
        int position = Math.random() * options.length > 1 ? 1 : 0; // only suitable for an array of 2 elements
        return options[position];
    }
}
