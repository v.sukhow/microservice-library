package com.spring;

public class Book {
    private int id;
    private String name;
    private int quantity;
    private int availableQuantity;

    public Book(int id, String name, int quantity, int availableQuantity) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.availableQuantity = availableQuantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(int availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    @Override
    public String toString() {
        return this.id + " - " + this.name + " - " + " - " + this.quantity
                + " - " + this.availableQuantity;
    }
}
