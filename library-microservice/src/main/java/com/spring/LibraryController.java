package com.spring;


import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
public class LibraryController {
    private Library library = new Library();

    @RequestMapping("/books/{id}")
    String getBookById(@PathVariable int id) throws IncorrectBookIdException {
        Book book = Arrays.stream(library.getBooks()).filter(b -> b.getId() == id)
                .findFirst().orElseThrow(() -> new IncorrectBookIdException("No book with such id"));
        return book.toString();
    }

    @RequestMapping("/books/{bookId}/decrease-number")
    public Object decreaseNumberBooks(@PathVariable int bookId) {
        Book bookFromList = Arrays.stream(library.getBooks()).filter(book -> book.getId() == bookId)
                .findFirst().get();
        bookFromList.setAvailableQuantity(bookFromList.getAvailableQuantity() - 1);
        return null;
    }


}
