package com.spring;

public class IncorrectBookIdException extends Exception{
    public IncorrectBookIdException(String errorMessage) {
        super(errorMessage);
    }
}
