package com.spring;

public class Library {
    private Book[] books;

    public Library() {
        this.books = new Book[] {
            new Book(1,"Spring", 10, 7),
            new Book(2,"Java", 10, 7),
            new Book(3,"Patterns", 10, 7),
        };
    }

    public Book[] getBooks() {
        return this.books;
    }

    public void setBooks(Book[] books) {
        this.books = books;
    }
}
