package com.spring;

public class User {
    private int id;
    private String name;
    private int numberBooks;

    public User(int id, String name, int numberBooks) {
        this.id = id;
        this.name = name;
        this.numberBooks = numberBooks;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberBooks() {
        return numberBooks;
    }

    public void setNumberBooks(int numberBooks) {
        this.numberBooks = numberBooks;
    }

    @Override
    public String toString() {
        return this.id + " - " + this.name + " - " + this.numberBooks;
    }
}
