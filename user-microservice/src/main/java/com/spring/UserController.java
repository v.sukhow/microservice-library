package com.spring;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
public class UserController {

    private User[] users = new User[] {
            new User(1, "Tom", 0),
            new User(2, "Sam", 3)
    };

    @RequestMapping("/users")
    public String getUsers() {
        return Arrays.toString(users);
    }

    @RequestMapping("users/{userId}/increase-number-books")
    public void increaseNumberBooks(@PathVariable int userId) {
        User user = Arrays.stream(users).filter(u -> u.getId() == userId).findFirst().get();
        user.setNumberBooks(user.getNumberBooks() + 1);
    }
}
